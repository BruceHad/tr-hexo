Hexo-TR
=======

Another Hexo blog theme.

This is version 2. Which is a change of design plus a few additional features to support IndieWeb style posting.

* Support h-card and authentication endpoints for IndieAuth
* Can add rel-me links in footer
* Posts support h-entry microformat

Installation
------------

Installing Hexo:

To install the hexo client:

    npm install hexo-cli -g
    hexo init blog-name
    cd blog-name
    npm install

Also need to install sass renderer for theme:

    npm install --save hexo-renderer-sass

To install the tr-hexo theme, copy the theme to the _blog-name/themes/tr-hexo_ folder in your hexo install.

    cd themes/
    git clone git@gitlab.com:BruceHad/tr-hexo.git
    cd ../

Update the hexo _config.yml_ file so that the theme is set to 'hexo-tr'. Amend any other settings you are interested in.

    theme: hexo-tr


Rename the tr-hexo _config.sample.yml_ file to just _config.yml_ file, and amend to your preferences.

    display_posts_on_index: true
    display_promo_section: true


Usage
-----

To generate static site:

    hexo generate

To run server:

    hexo server

Or if you need a specific port:

    hexo server -p 8080

RSS Feed
--------

If you want an RSS feed, try installing [Hexo Generator Feed](https://github.com/hexojs/hexo-generator-feed).

Sitemap
-------

To automatically generate a sitemap, install hexo-generator sitemap, update the site's config.yml and create the sitemap_template.xml file. See [hexo-generator-sitemap](https://github.com/hexojs/hexo-generator-sitemap/) for more details.

You can update your theme's config.yml to set sitemap: true to display a link to your sitemap in your footer.