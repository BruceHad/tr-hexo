self.addEventListener('install', function(e) {
  e.waitUntil(
    // do something
    caches.open('treerock').then(function(cache) {
      return cache.addAll([
        '/',
        '/index.html',
        '/css/style.css',
        '/scripts/ww.js'
      ]);
    })
  );
});