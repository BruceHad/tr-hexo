v1

-[x] Update README with instructions
-[x] Change layout of post.ejs
-[x] Update fonts, colours and layout of header and footer
-[x] Amend Index page
-[x] Update navigation and include about, archive and rss feeds
-[x] Fix pages
-[x] Update repository to remove the config file and use a sample instead.
-[x] Styles similar to treerock.me
-[x] Convert to flexbox for vertical, stacked layout
-[x] Remove 'grid' css and use flexbox
-[x] Update colours, headings and fonts
-[x] Add in CSS styling for figure and figurecaption.
-[x] Change how multiple categories are displayed on index page.

v2

-[x] Update layout.ejs and amend structure of css
-[x] Design header elements
-[x] Fix body so multiple elements can be displayed in the 'news' section.
-[x] Give option for to display archive (recent posts etc) on index
-[x] Style archives
-[x] Design body elements
-[x] Design footer elements
-[x] Add archive link to bottom of posts
-[x] Fix navigation links and see if I can make a permanent highlight for 'current' page
-[x] Fix readme instructions
-[x] Make 'promo' section at top of main page (optional)
-[x] Set up proper colour scheme
-[x] Fix pagination layout/seperate the 'grid' css
-[x] Make header linkable
-[x] Fix config.sample.yml
-[x] Fix logo (temporary replacement)
-[x] Navigation hover animation
-[x] Tidy up 'recent posts' list.
-[x] Tidy up 'Page' and single 'Post' layout
-[x] Tidy up 'Archive' page
-[x] Parameters for: sub-heading, information and promo section
-[x] Fix config.sample.yml
-[x] Add permalink for posts
-[x] Update footer
-[x] Fix colours

v2.1

Indie Web Stuff:

-[x] Set up microformat markdown
-[x] Set up h-card option & authentication endpoints.
-[x] Add optional rel-me links

Design and Layout:

-[x] Refactor for C3 style css
-[x] Show post meta data
-[x] Improve image handling on posts/pages
-[x] Fix logo heading and alignment of headings/???????????
-[x] fix issue with posts (trees)
-[x] fix navigation css on small screens
-[x] change 'info' on header

2.2

PWA:

-[ ] Make it 'installable'
  -[ ] Add webworker to cache contents
  -[ ] Add manifest

Fixes:
-[x] Footer text
-[x] Margins
-[x] Add feed to header
-[x] Fix link colour in footer
-[x] Restyle navigation on small screens

Other:
-[ ] Switch to FuturaRenner
-[x] Add in analytics option
-[x] Add sitemap